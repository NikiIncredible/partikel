package de.Zayon.Community.Utils;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;

public class Items {
	
	@SuppressWarnings("deprecation")
	public static ItemStack createItem(Material material, int subid, String displayname) {
		
		ItemStack item = new ItemStack(material, 1,(short) subid);
		ItemMeta mitem = item.getItemMeta();
		mitem.setDisplayName(displayname);
	    mitem.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
	    mitem.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
	    mitem.addItemFlags(ItemFlag.HIDE_ENCHANTS);
	    mitem.addItemFlags(ItemFlag.HIDE_DESTROYS);
		item.setItemMeta(mitem);
		
		return item;
	}
	
	public static ItemStack createAir() {
		ItemStack item = new ItemStack(Material.AIR, 1,(short) 0);
		return item;
	}
		
		@SuppressWarnings("deprecation")
		public static ItemStack createItemA(Material material, int subid, String displayname, int anzahl) {
			
			ItemStack item = new ItemStack(material, anzahl,(short) subid);
			ItemMeta mitem = item.getItemMeta();
			mitem.setDisplayName(displayname);
		    mitem.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
		    mitem.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		    mitem.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		    mitem.addItemFlags(ItemFlag.HIDE_DESTROYS);
			item.setItemMeta(mitem);
			
			return item;
		}
		
		public static ItemStack createNoName(Material material, int subid) {
			
			ItemStack item = new ItemStack(material, 1,(short) subid);
			ItemMeta mitem = item.getItemMeta();
			item.setItemMeta(mitem);
			
			return item;
		}
	
public static ItemStack Arrow(Material material, int subid, String displayname) {
		
		ItemStack item = new ItemStack(material, 8,(short) subid);
		ItemMeta mitem = item.getItemMeta();
		mitem.setDisplayName(displayname);
		item.setItemMeta(mitem);
		
		return item;
	}
	
	
		
		public static ItemStack createItem64(Material material, int subid, String displayname) {
			
			ItemStack item = new ItemStack(material, 64,(short) subid);
			ItemMeta mitem = item.getItemMeta();
			mitem.setDisplayName(displayname);
			item.setItemMeta(mitem);
			
			return item;
		}
	
	public static ItemStack createItemLore(Material material, int subid, String displayname, String lore) {
		
			ItemStack item = new ItemStack(material, 1,(short) subid);
			ItemMeta mitem = item.getItemMeta();
			ArrayList<String> Lore =  new ArrayList<String>();
			Lore.add(lore);
			mitem.setLore(Lore);
			mitem.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
			mitem.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
	
			mitem.setDisplayName(displayname);
			item.setItemMeta(mitem);
			
			
			
			return item;
	}
	
	public static ItemStack createItemLore(Material material, int subid, String displayname, ArrayList<String> lore) {
		
		ItemStack item = new ItemStack(material, 1,(short) subid);
		ItemMeta mitem = item.getItemMeta();
		mitem.setLore(lore);
		mitem.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
		mitem.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);

		mitem.setDisplayName(displayname);
		item.setItemMeta(mitem);
		
		
		
		return item;
	}
	
	public static ItemStack createItemLore(Material material, int subid, String displayname, ArrayList<String> lore, int amount) {
		
		ItemStack item = new ItemStack(material, amount,(short) subid);
		ItemMeta mitem = item.getItemMeta();
		mitem.setLore(lore);
		mitem.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		mitem.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);

		mitem.setDisplayName(displayname);
		item.setItemMeta(mitem);
		
		
		
		return item;
	}
	
	public static ItemStack createItemLore(Material material, int subid, String displayname, String lore, int amount) {
		
		ItemStack item = new ItemStack(material, amount,(short) subid);
		ItemMeta mitem = item.getItemMeta();
		ArrayList<String> Lore =  new ArrayList<String>();
		Lore.add(lore);
		mitem.setLore(Lore);
		mitem.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		mitem.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);

		mitem.setDisplayName(displayname);
		item.setItemMeta(mitem);
		
		
		
		return item;
	}
	
	@SuppressWarnings("deprecation")
	public static ItemStack createShoes(String displayname, Color color) {
		
		ItemStack i = new ItemStack(Material.LEATHER_BOOTS, 1);
		LeatherArmorMeta im = (LeatherArmorMeta) i.getItemMeta();
		im.setDisplayName(displayname);
		im.setColor(color);
		im.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        im.spigot().setUnbreakable(true);
		i.setItemMeta(im);
		
		return i;
	}
	
    @SuppressWarnings("deprecation")
	public static ItemStack createChest(String displayname, Color color) {
		
		ItemStack i = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
		LeatherArmorMeta im = (LeatherArmorMeta) i.getItemMeta();
		im.setDisplayName(displayname);
		im.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        im.spigot().setUnbreakable(true);
		im.setColor(color);
		i.setItemMeta(im);
		
		return i;
	}
    
@SuppressWarnings("deprecation")
public static ItemStack createHose(String displayname, Color color) {
		
		ItemStack i = new ItemStack(Material.LEATHER_LEGGINGS, 1);
		LeatherArmorMeta im = (LeatherArmorMeta) i.getItemMeta();
		im.setDisplayName(displayname);
		im.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        im.spigot().setUnbreakable(true);
		im.setColor(color);
		i.setItemMeta(im);
		
		return i;
	}
	
@SuppressWarnings("deprecation")
public static ItemStack createHelm(String displayname, Color color) {
		
		ItemStack i = new ItemStack(Material.LEATHER_HELMET, 1);
		LeatherArmorMeta im = (LeatherArmorMeta) i.getItemMeta();
		im.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        im.spigot().setUnbreakable(true);
		im.setDisplayName(displayname);
		im.setColor(color);
		i.setItemMeta(im);
		
		return i;
}


	@SuppressWarnings("deprecation")
	public static ItemStack createItemEnch1(Material material, int subid, String displayname, Enchantment enchantment) {
	
		ItemStack itemE = new ItemStack(material, 1,(short) subid);
        ItemMeta testEnchantMeta = itemE.getItemMeta();
        testEnchantMeta.addEnchant(enchantment, 1, true);
        testEnchantMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        testEnchantMeta.spigot().setUnbreakable(true);
        testEnchantMeta.setDisplayName(displayname);
        itemE.setItemMeta(testEnchantMeta);
        
        return itemE;
    
	}
	
	@SuppressWarnings("deprecation")
	public static ItemStack createItemEnch2(Material material, int subid, String displayname, Enchantment enchantment) {
		
		ItemStack itemE2 = new ItemStack(material, 1,(short) subid);
        ItemMeta testEnchantMeta = itemE2.getItemMeta();
        testEnchantMeta.addEnchant(enchantment, 1, true);
        testEnchantMeta.setDisplayName(displayname);
        testEnchantMeta.spigot().setUnbreakable(true);
        itemE2.setItemMeta(testEnchantMeta);
        
        return itemE2;
	}
	
	public static ItemStack Skull(String Display, Material material, int subid, String Owner)
	  {
	    ItemStack stack = new ItemStack(material, 1, (short) subid);
	    SkullMeta metas = (SkullMeta)Bukkit.getItemFactory().getItemMeta(Material.SKULL_ITEM);
	    metas.setOwner(Owner);
	    metas.setDisplayName(Display);
	    stack.setItemMeta(metas);
	    return stack;
	  }
    
	public static ItemStack Coin(int amount) {
		
		ArrayList<String> lore = new ArrayList<>();
		
		ItemStack itemE2 = new ItemStack(Material.WATCH, amount,(short) 0);
        ItemMeta Meta = itemE2.getItemMeta();
        Meta.setDisplayName("�6M�nze");
        lore.add("�7ID 006");
        Meta.setLore(lore);
        itemE2.setItemMeta(Meta);
        
        return itemE2;
	}
	
	
	
     //public static ItemStack createItemLore(Material material, int subid, String displayname, String lore) {
		
		//ItemStack item = new ItemStack(material, 1,(short) subid);
		//ItemMeta mitem = item.getItemMeta();
		//mitem.setDisplayName(displayname);
		//mitem.setLore(arg0);
		//item.setItemMeta(mitem);
		
		//return item;
	//}
	
	
	
	}
	

    //player.getInventory().addItem(setSkin(new ItemStack(Material.SKULL_ITEM, 1, (byte) 3), "pizzafreak08"));