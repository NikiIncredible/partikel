package de.Zayon.Community.Utils;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.minecraft.server.v1_12_R1.IChatBaseComponent;
import net.minecraft.server.v1_12_R1.PacketPlayOutChat;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class ClickText {

    public static void klick(Player player, String text, String klicktext, String command) {

//		IChatBaseComponent chat = ChatSerializer.a("{\"text\":\"" + text + "\" ,\"extra\":[{ \"text\":\"" + klicktext + "\"},\"clickEvent\":{\"action\":\"run_command\",\"value\":\"" + command + "\" }]   }");
//
//
//		PacketPlayOutChat packet = new PacketPlayOutChat(chat);
//		((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);

        IChatBaseComponent chat1 = IChatBaseComponent.ChatSerializer.a("{\"text\":\""+ text +"\",\"extra\":[{\"text\":\""+ klicktext +"\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\""+ command +"\" }}}]}");
        PacketPlayOutChat packet1 = new PacketPlayOutChat(chat1);
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet1);
    }

    public static void klickText(Player player, String text, String command) {

        net.md_5.bungee.api.chat.TextComponent tc = new net.md_5.bungee.api.chat.TextComponent();
        tc.setText(text);
        tc.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
        player.spigot().sendMessage(tc);
    }

    public static void hoverText(Player player, String text, String hovertext, String command) {

        net.md_5.bungee.api.chat.TextComponent tc = new net.md_5.bungee.api.chat.TextComponent();
        tc.setText(text);
        tc.setHoverEvent(new HoverEvent(net.md_5.bungee.api.chat.HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(hovertext).create()));
        tc.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
        player.spigot().sendMessage(tc);
    }
}
