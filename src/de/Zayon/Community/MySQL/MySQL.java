package de.Zayon.Community.MySQL;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.sql.*;

public class MySQL
{
  public static String username = "main";
  public static String password = "39cP7NC29OHRwAq5";
  public static String database = "zayon_main";
  public static String host = "localhost";
  public static String port = "3306";
  public static Connection con;
  
  public MySQL(String user, String pass, String host2, String dB) {}
  
  public static void connect()
  {
    if (!isConnected()) {
      try
      {
    	  con = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database + "?user=" + username + "&password=" + password + "&autoReconnect=true");
        Bukkit.getConsoleSender().sendMessage("§aMySQL MySQL connected");
      }
      catch (SQLException e)
      {
        e.printStackTrace();
      }
    }
  }
  
  public static void close()
  {
    if (isConnected()) {
      try
      {
        con.close();
        Bukkit.getConsoleSender().sendMessage("§aMySQL MySQL disconnected");

      }
      catch (SQLException e)
      {
        e.printStackTrace();
      }
    }
  }
  
  public static boolean isConnected()
  {
    return con != null;
  }
  public static void createTable()
  {
    if (isConnected()) {
      try
      {

        con.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS users_coins(uuid VARCHAR(255), coins Int(30), magiccoins Int(30), mc_name VARCHAR(16))");
        
        Bukkit.getConsoleSender().sendMessage("�4MySQL MySQL Table created");
      }
      catch (SQLException e)
      {
        e.printStackTrace();
      }
    }
  }

  
  public static void update(String qry)
  {
    if (isConnected()) {
      try
      {
        con.createStatement().executeUpdate(qry);
      }
      catch (SQLException e)
      {
        e.printStackTrace();
      }
    }
  }
  
  public static ResultSet getResult(String qry)
  {
    ResultSet rs = null;
    try
    {
      Statement st = con.createStatement();
      rs = st.executeQuery(qry);
    }
    catch (SQLException e)
    {
      connect();
      System.err.println(e);
    }
    return rs;
  }
  
  public static File getMySQLFile()
  {
    return new File("plugins/Natura", "MySQL.yml");
  }
  
  public static FileConfiguration getMySQLFileConfiguration()
  {
    return YamlConfiguration.loadConfiguration(getMySQLFile());
  }
  

  

}
