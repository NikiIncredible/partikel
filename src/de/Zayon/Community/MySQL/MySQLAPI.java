package de.Zayon.Community.MySQL;

import de.Zayon.Community.Utils.NameFetcher;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;


public class MySQLAPI {
	
	
		  public static boolean playerExists(String uuid)
		  {
			try
			{
			  ResultSet rs = MySQL.getResult("SELECT * FROM users_coins WHERE id='" + uuid + "'");
			  if (rs.next()) {
				return rs.getString("id") != null;
			  }
			  return false;
			}
			catch (SQLException e)
			{
			  e.printStackTrace();
			}
			return false;
		  }

		  public static void createPlayer(String uuid)
		  {
			MySQL.update("INSERT INTO users_coins (id, coins) VALUES ('" + uuid +"', '250');");
		  }

		  public static int getCoins(Player player){
			  int i = 0;
			  String uuid = player.getUniqueId().toString();
			  try
			  {
				ResultSet rs = MySQL.getResult("SELECT coins FROM users_coins WHERE id='" + uuid + "'");
				if ((rs.next()) && (Integer.valueOf(rs.getString("coins")) == null)) {}
				 i = Integer.valueOf(rs.getInt("coins"));
			  }
			  catch (SQLException e)
			  {
				e.printStackTrace();
			  }
			return i;

		  }
	  
		  public static int getCoins(String uuid){
			  int i = 0;
			  try
			  {
				ResultSet rs = MySQL.getResult("SELECT coins FROM users_coins WHERE id='" + uuid + "'");
				if ((rs.next()) && (Integer.valueOf(rs.getString("coins")) == null)) {}
				 i = Integer.valueOf(rs.getInt("coins"));
			  }
			  catch (SQLException e)
			  {
				e.printStackTrace();
			  }
			return i;

		  }
	  
	  	public static void SetCoins(Player player, int amount){
	 
			CoinsChangeEvent cce = new CoinsChangeEvent(player, amount);
			Bukkit.getPluginManager().callEvent(cce);
			if (!cce.isCancelled()) {
				MySQL.update("UPDATE users_coins SET coins='" + amount + "' WHERE id ='"+ player.getUniqueId().toString() +"'");
			}
	  	}
	  
	  	public static void SetCoins(String uuid, int amount){

		MySQL.update("UPDATE users_coins SET coins='" + amount + "' WHERE id ='"+ uuid +"'");


		    }
	    
	    public static void addCoins(Player player, int coins) {
	    	
	    	int i = getCoins(player);
	    	
	    	SetCoins(player, i + coins);
	    	
	    }
	    
	    public static void addCoins(String uuid, int coins) {
	    	
	    	int i = getCoins(uuid);
	    	
	    	SetCoins(uuid, i + coins);
	    	
	    }
	    
	    public static void removeCoins(Player player, int coins) {
	    	int i = getCoins(player);
	    	
	    	SetCoins(player, i - coins);
	    }
	    
	    public static void removeCoins(String uuid, int coins) {
			int i = getCoins(uuid);

			SetCoins(uuid, i - coins);
		}

}
